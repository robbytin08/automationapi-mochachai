# Testing Automation API Using Node.js, Mocha and Chai

Prequisites: Install Node.js

Step to use this project:
1. Clone
    *   git clone https://gitlab.com/robbytin08/automationapi-mochachai.git
2. npm install
3. Change envsample to .env
    *    mv envsample .env
4. npm run test-api 
5. npm run reports

https://drive.google.com/file/d/1jD-9vo7e83w1BnPLKMyfNGAo28oKSX1l/view?usp=sharing

